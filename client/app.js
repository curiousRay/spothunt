/**
 * @fileOverview 微信小程序的入口文件
 */

var qcloud = require('./vendor/wafer2-client-sdk/index');
var config = require('./config');

App({
    /**
     * 小程序初始化时执行，我们初始化客户端的登录地址，以支持所有的会话操作
     */
    onLaunch() {
        qcloud.setLoginUrl(config.service.loginUrl);
        var that = this
        //获取屏幕尺寸，放到全局结构中
        wx.getSystemInfo({
          success: function (res) {
            that.globalData.scHeight = res.windowHeight
            that.globalData.scWidth = res.windowWidth
          },
        })
        console.log(this.globalData.scWidth)
        console.log(this.globalData.scHeight)
  },
  
      globalData: {
        userInfo: null,
        scWidth: 0,            //全局的屏幕尺寸，已经去掉了上边的标题栏
        scHeight: 0,
    g_sessionkey: null,
    g_openid: null
      },

      getUserInfo: function (cb) {
        var that = this
        if (this.globalData.personInfo) {
          typeof cb == "function" && cb(this.globalData.personInfo)
        } else {
          //调用登录接口
          wx.login({
            success: function () {
              wx.getUserInfo({
                success: function (res) {
                  that.globalData.personInfo = res.userInfo
                  typeof cb == "function" && cb(that.globalData.personInfo)
                }
              })
            }
          })
        }
      }

})