/**
 * @fileOverview 演示会话服务和 WebSocket 信道服务的使用方式
 */

// 引入 QCloud 小程序增强 SDK
var qcloud = require('../../vendor/wafer2-client-sdk/index');

// 引入配置
var config = require('../../config');

// 显示繁忙提示
var showBusy = text => wx.showToast({
    title: text,
    icon: 'loading',
    duration: 10000
});

// 显示成功提示
var showSuccess = text => wx.showToast({
    title: text,
    icon: 'success'
});

// 显示失败提示
var showModel = (title, content) => {
    wx.hideToast();

    wx.showModal({
        title,
        content: JSON.stringify(content),
        showCancel: false
    });
};
var imgUrl = ''; //make 'imgUrl' defined.
var currating = 0;
/**
 * 使用 Page 初始化页面，具体可参考微信公众平台上的文档
 */
Page({

    /**
     * 初始数据，我们把服务地址显示在页面上
     */
    data: {
        loginUrl: config.service.loginUrl,
        requestUrl: config.service.requestUrl,
        tunnelUrl: config.service.tunnelUrl,
        uploadUrl: config.service.uploadUrl,
        tunnelStatus: 'closed',
        tunnelStatusText: {
            closed: '已关闭',
            connecting: '正在连接...',
            connected: '已连接'
        },
        imgUrl: '',
        flag: [0, 0, 0],
        startext: ['', '', ''],
        stardata: [1, 2, 3, 4, 5],
        array: [],
        id: ''
    },

    /**
     * 点击「登录」按钮，测试登录功能
     */
    doLogin() {
        showBusy('正在登录');

        // 登录之前需要调用 qcloud.setLoginUrl() 设置登录地址，不过我们在 app.js 的入口里面已经调用过了，后面就不用再调用了
        qcloud.login({
            success(result) {
                showSuccess('登录成功');
                console.log('登录成功', result);
            },

            fail(error) {
                showModel('登录失败', error);
                console.log('登录失败', error);
            }
        });
    },

    /**
     * 点击「登录」按钮，测试登录功能
     */
    bindGetUserInfo: function (e) {

      showBusy('正在登录');

      var that = this;
      var userInfo = e.detail.userInfo;

      // 查看是否授权
      wx.getSetting({
        success: function (res) {
          if (res.authSetting['scope.userInfo']) {

            // 检查登录是否过期
            wx.checkSession({
              success: function () {
                // 登录态未过期
                showSuccess('登录成功');
                console.log('登录成功', userInfo);
              },

              fail: function () {
                qcloud.clearSession();
                // 登录态已过期，需重新登录
                var options = {
                  encryptedData: e.detail.encryptedData,
                  iv: e.detail.iv,
                  userInfo: userInfo
                }
                that.getWxLogin(options);
              },
            });
          } else {
            showModel('用户未授权', e.detail.errMsg);
          }
        }
      });
    },

    getWxLogin: function (options) {
      var that = this;

      wx.login({
        success: function (loginResult) {
          var loginParams = {
            code: loginResult.code,
            encryptedData: options.encryptedData,
            iv: options.iv,
          }
          qcloud.requestLogin({
            loginParams, success() {
              showSuccess('登录成功');
              console.log('登录成功', options.userInfo);
            },
            fail(error) {
              showModel('登录失败', error)
              console.log('登录失败', error)
            }
          });
        },
        fail: function (loginError) {
          showModel('登录失败', loginError)
          console.log('登录失败', loginError)
        },
      });
    },

    /**
     * 点击「清除会话」按钮
     */
    clearSession() {
        // 清除保存在 storage 的会话信息
        qcloud.clearSession();
        showSuccess('会话已清除');
    },

    /**
     * 点击「请求」按钮，测试带会话请求的功能
     */
    doRequest() {
        showBusy('正在请求');

        // qcloud.request() 方法和 wx.request() 方法使用是一致的，不过如果用户已经登录的情况下，会把用户的会话信息带给服务器，服务器可以跟踪用户
        qcloud.request({
            // 要请求的地址
            url: this.data.requestUrl,

            // 请求之前是否登陆，如果该项指定为 true，会在请求之前进行登录
            login: true,

            success(result) {
                showSuccess('请求成功完成');
                console.log('request success', result);
            },

            fail(error) {
                showModel('请求失败', error);
                console.log('request fail', error);
            },

            complete() {
                console.log('request complete');
            }
        });
    },

    doUpload () {
        var that = this

        wx.chooseImage({
          count: 1,
          sizeType: ['compressed'],
          sourceType: ['album', 'camera'],
          success: function(res){
            var tempFilePath = res.tempFilePaths[0]
            
            console.log("testhere")
            console.log(tempFilePath)
            
            
            wx.uploadFile({
              url: that.data.uploadUrl,
              filePath: tempFilePath,
              name: 'file',
              /*formData: {
                portal_id: portal_id,
                user_openid: wx.getStorageSync('openid'),
                rate: currating
              },*/
              success: function(res){
                showSuccess('上传图片成功')
                //console.log(portal_id)
                console.log("flag1")
                console.log(res)
                res = JSON.parse(res.data)
                console.log("flag2")
                console.log(res)
                that.setData({
                    imgUrl: res.data.imgUrl
                })
              },

              fail: function(e) {
                console.error(e)
              }
            })

          },
          fail: function(e) {
            console.error(e)
          }
        })
    },

    previewImage () {
        wx.previewImage({
            current: this.data.imgUrl,
            urls: [this.data.imgUrl]
        })
    },

    onLoad: function () {
      var array = (wx.getStorageSync('array') || []);
      var flag = (wx.getStorageSync('flag') || []);
      console.log(flag)
      var id
      if (flag == 0) {
         id = (wx.getStorageSync('id') || []);
      }
      else {
         id = (wx.getStorageSync('portalid') || []);
      }
      this.setData({
        array: array,
        id: id
      })
      //console.log(this.data.array);
      //console.log(this.data.id);
      console.log(array[id].id);
    },

    changeColor: function (e) {
      console.log(e.currentTarget.dataset)
      var index = e.currentTarget.dataset.index;
      var num = e.currentTarget.dataset.no;
      var a = 'flag[' + index + ']';
      var b = 'startext[' + index + ']';
      var that = this;
      if (num == 1) {
        that.setData({
          [a]: 1,
          [b]: '厌恶'
        });
      } else if (num == 2) {
        that.setData({
          [a]: 2,
          [b]: '讨厌'
        });
      } else if (num == 3) {
        that.setData({
          [a]: 3,
          [b]: '无感'
        });
      } else if (num == 4) {
        that.setData({
          [a]: 4,
          [b]: '喜欢'
        });
      } else if (num == 5) {
        that.setData({
          [a]: 5,
          [b]: '点赞'
        });
      }
    },

    onShareAppMessage: function () {
      return {
        title: "景点推荐"
      }
    },

    onPullDownRefresh: function () {
      wx.navigateTo({
        url: "/pages/outdex/outdex",
      })
    }

});
