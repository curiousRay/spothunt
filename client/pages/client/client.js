var util = require('../../utils/util.js')
var qcloud = require('../../vendor/wafer2-client-sdk/index')

Page({
  data: {
    userInfo: {},
    logged: false,
    takeSession: false,
    requestResult: '',
    motto:'the world is not what you see,it is time for you to remove',
    clientlist:[
      {
        typeId:0,
        name:'收藏',
        url:'/pages/collect/collect',
        imageurl:'../../images/person/navs-heart.svg'
      },
      {
        typeId:1,
        name:'历史',
        url:'/pages/history/history',
        imageurl:'../../images/person/navs-history.svg'
      },
      {
        typeId:2,
        name:'想去',
        url:'/pages/want/want',
        imageurl:'../../images/person/navs-plane.svg'
      }
    ],
    orderlist:[
      {
        typeId:0,
        name:'我的相册',
        url:'/pages/albumn/albumn',
        imageurl:'../../images/person/order-album.svg'
      },
      {
        typeId:1,
        name:'我的金句',
        url:'/pages/saying/saying',
        imageurl:'../../images/person/order-pencil.svg'
      },
      {
        typeId:2,
        name:'我的任务',
        url:'/pages/mission/mission',
        imageurl:'../../images/person/order-file.svg'
      }
    ],

  },

  // 用户登录示例
  login: function () {
    if (this.data.logged) return

    util.showBusy('正在登录')
    var that = this

    // 调用登录接口
    qcloud.login({
      success(result) {
        if (result) {
          util.showSuccess('登录成功');
          console.log(result)
          that.setData({
            userInfo: result,
            logged: true
          })
        } else {
          // 如果不是首次登录，不会返回用户信息，请求用户信息接口获取
          qcloud.request({
            url: config.service.requestUrl,
            login: true,
            success(result) {
              util.showSuccess('登录成功')
              console.log(result)
              that.setData({
                userInfo: result.data.data,
                logged: true
              })
            },

            fail(error) {
              util.showModel('请求失败', error)
              console.log('request fail', error)
            }
          })
        }
      },

      fail(error) {
        util.showModel('登录失败', error)
        console.log('登录失败', error)
      }
    })
  },

  bindGetUserInfo: function (e) {
    if (this.data.logged) return;

    util.showBusy('正在登录');

    var that = this;
    var userInfo = e.detail.userInfo;
    console.log(userInfo)
    console.log(e)
//此处通过login来获取openid
wx.login({
  success: function (r) {
    wx.request({
      url: 'https://api.weixin.qq.com/sns/jscode2session',
      method: 'GET',
      data: {
        appId: 'wxb8b4f55261fa1c6a',
        secret: 'f9faa8cbccad28495f561b82caccae1e',
        js_code: r.code,
        grant_type: 'authorization_code'  
      },
      success: function (res) {
        console.log(res)//session_key和openid
        wx.setStorageSync('userInfo', e.detail.userInfo)
        wx.setStorageSync('sessionkey', res.data.session_key)
        wx.setStorageSync('openid', res.data.openid)
        //console.log('userinfo is'+wx.getStorageSync('userInfo'))
        //console.log('skey is'+wx.getStorageSync('sessionkey'))
        //console.log('openid is'+wx.getStorageSync('openid'))
      
      }
    })

  }
})
    // 查看是否授权
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {

          // 检查登录是否过期
          wx.checkSession({
            success: function () {
              // 登录态未过期
              util.showSuccess('登录成功');
              that.setData({
                userInfo: userInfo,
                logged: true
              })
            },

            fail: function () {
              qcloud.clearSession();
              // 登录态已过期，需重新登录
              var options = {
                encryptedData: e.detail.encryptedData,
                iv: e.detail.iv,
                userInfo: userInfo
              }
              that.doLogin(options);
            },
          });
        } else {
          util.showModel('用户未授权', e.detail.errMsg);
        }
      }
    });
  },

  doLogin: function (options) {
    var that = this;

    wx.login({
      success: function (loginResult) {
        var loginParams = {
          code: loginResult.code,
          encryptedData: options.encryptedData,
          iv: options.iv,
        }
        qcloud.requestLogin({
          loginParams, success() {
            util.showSuccess('登录成功');

            that.setData({
              userInfo: options.userInfo,
              logged: true
            })
          },
          fail(error) {
            util.showModel('登录失败', error)
            console.log('登录失败', error)
          }
        });
      },
      fail: function (loginError) {
        util.showModel('登录失败', loginError)
        console.log('登录失败', loginError)
      },
    });
  },

  // 切换是否带有登录态
  switchRequestMode: function (e) {
    this.setData({
      takeSession: e.detail.value
    })
    this.doRequest()
  },

  doRequest: function () {
    util.showBusy('请求中...')
    var that = this
    var options = {
      url: config.service.requestUrl,
      login: true,
      success(result) {
        util.showSuccess('请求成功完成')
        console.log('request success', result)
        that.setData({
          requestResult: JSON.stringify(result.data)
        })
      },
      fail(error) {
        util.showModel('请求失败', error);
        console.log('request fail', error);
      }
    }
    if (this.data.takeSession) {  // 使用 qcloud.request 带登录态登录
      qcloud.request(options)
    } else {    // 使用 wx.request 则不带登录态
      wx.request(options)
    }
  },
  
  tap:function(event){
    console.log(event.currentTarget)
    wx.navigateTo({
      url: event.currentTarget.dataset.clienturl
    })
  }
})