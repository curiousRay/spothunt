const app = getApp()
var array = []

var location = function (_that) {
  wx.getLocation({
    type: "gcj02",
    success: (res) => {
      var result = res
      var that = _that;
      wx.request({
        url: 'https://vwaasebj.qcloud.la/weapp/getres?lng=' + res.longitude + '&lat=' + res.latitude,
        success: function (res) {
          console.log(res);
          array = res.data.data;
          console.log(array.length);
          var len = array.length;
          if (len < 5) {
            for (var i = 1; i <= (5 - len); i++) {
              array.push('');
            }
          }
          console.log(array);

          that.setData({
            longitude: result.longitude,
            latitude: result.latitude,
            markers: [
              {
                id: 0,
                latitude: result.latitude,
                longitude: result.longitude,
                width: 50,
                height: 50,
                iconPath: '', //默认气泡样式
                title: "起始点"
              },
              {
                id: 1,
                latitude: (array[0] != '') ? array[0].lat : '',
                longitude: (array[0] != '') ? array[0].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[0] != '') ? array[0].title : ''
              },
              {
                id: 2,
                latitude: (array[1] != '') ? array[1].lat : '',
                longitude: (array[1] != '') ? array[1].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[1] != '') ? array[1].title : ''
              },
              {
                id: 3,
                latitude: (array[2] != '') ? array[2].lat : '',
                longitude: (array[2] != '') ? array[2].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[2] != '') ? array[2].title : ''
              },
              {
                id: 4,
                latitude: (array[3] != '') ? array[3].lat : '',
                longitude: (array[3] != '') ? array[3].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[3] != '') ? array[3].title : ''
              },
              {
                id: 5,
                latitude: (array[4] != '') ? array[4].lat : '',
                longitude: (array[4] != '') ? array[4].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[4] != '') ? array[4].title : ''
              },
              {
                id: 6,
                latitude: (array[5] != '') ? array[5].lat : '',
                longitude: (array[5] != '') ? array[5].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[5] != '') ? array[5].title : ''
              },
              {
                id: 7,
                latitude: (array[6] != '') ? array[6].lat : '',
                longitude: (array[6] != '') ? array[6].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[6] != '') ? array[6].title : ''
              },
              {
                id: 8,
                latitude: (array[7] != '') ? array[7].lat : '',
                longitude: (array[7] != '') ? array[7].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[7] != '') ? array[7].title : ''
              },
              {
                id: 9,
                latitude: (array[8] != '') ? array[8].lat : '',
                longitude: (array[8] != '') ? array[8].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[8] != '') ? array[8].title : ''
              },
              {
                id: 10,
                latitude: (array[9] != '') ? array[9].lat : '',
                longitude: (array[9] != '') ? array[9].lng : '',
                width: 50,
                height: 50,
                iconPath: '1.gif',
                title: (array[9] != '') ? array[9].title : ''
              }
            ]
          })
          console.log(that.data);
        },
        fail: function (res) {
          console.log(res);
        }
      })
      console.log(that.data);
    }
  });
}

var event

Page({
  onLoad: function () {
    var _that = this;
    location(_that);
  },

  onReady: function () {
    var _that = this;
    event = setInterval(function () {
      location(_that);
    }, 15000);
  },
  
  /*onHide:function(){
    setTimeout(function(){
      clearInterval(event)
    },3000)
  },

  onShow:function(){
    this.onLoad();
  },*/
  
  onUnload: function () {
    setTimeout(function () {
      clearInterval(event)
    }, 3000)
  },

  data: {
    //latitude: 23.099994,
    //longitude: 113.324520,
    // 调试时在此赋值
    scale: 15,

    regionchange(e) {
      console.log("regionchange===" + e.type)
    },

    controls: [{
      id: 1,
      iconPath: '/images/icons/plus.png',
      position: {
        left: app.globalData.scWidth * 0.9 - 10,
        top: app.globalData.scHeight * 0.77,
        width: 20,
        height: 20
      },
      clickable: true
    },
    {
      id: 2,
      iconPath: '/images/icons/minus.png',
      position: {
        left: app.globalData.scWidth * 0.9 - 10,
        top: app.globalData.scHeight * 0.77 + 25,
        width: 20,
        height: 20
      },
      clickable: true
    },
    {
      id: 3,
      iconPath: '/images/icons/camera.png',
      position: {
        left: app.globalData.scWidth * 0.9 - 20,
        top: app.globalData.scHeight * 0.76 - 45,
        width: 40,
        height: 40
      },
      clickable: true
    }
    ],
    //markers:[]
    list: []
  },

 mapControlTap: function (e) {
   switch (e.controlId) {
     case 1: { // scale加
         var that = this;
         if (this.data.scale >= 5 && this.data.scale <= 19) {
           that.setData({
             scale: ++this.data.scale
           })
         } else {
           that.setData({
             scale: 20
           })
         }
         break;
      }
      //console.log("此时scale===" + this.data.scale)
      
       
     case 2: { // scale减
       var that = this;
   
       if (this.data.scale > 5 && this.data.scale <= 20) {
         that.setData({
           scale: --this.data.scale
         })
       } else {
         that.setData({
           scale: 5
         })
       }
       //console.log("此时scale===" + this.data.scale)
       break;
     }

     case 3: { // 上传图片
       wx.chooseVideo({
         sourceType: ['camera'],
         //录制视频最大时长
         maxDuration: 3,
         //摄像头
         camera: ['back'],
         //这里返回的是tempFilePaths并不是tempFilePath
         success: function (res) {
           console.log(res)
           //res里面有width,height,duration,tempFilePath
           //console.log(gifshot.isSupported());
           //gifshot.isSupported()
         },
         fail: function (e) {
           console.log(e)
         }
       })
       break;
     }
   }
 },

 markertap(e) {
   console.log(e.markerId)
   console.log(this.data.latitude)
   console.log(this.data.longitude)

   var flag
   if (e.markerId === 0) {
     var that = this
     for (var i = 0; i < 6; i++) {
       var title = 'list[' + i + ']'
       that.setData({
         [title]: array[i].title
       })
     }
     flag = 0;
     console.log(flag)
     wx.setStorageSync('flag', flag);
     wx.setStorageSync('array', array);
     wx.showActionSheet({
       itemList: that.data.list,
       success: function (res) {
         //console.log(res.tapIndex)
         var id = res.tapIndex;
         console.log(id)
         wx.setStorageSync('id', id);
         wx.navigateTo({
           url: '/pages/outdex/outdex',
         })
       },
       fail: function (res) {
         console.log(res.errMsg)
       }
     })
   } else {
     var flag = 1;
     var id = e.markerId - 1;
     wx.setStorageSync('flag', flag);
     wx.setStorageSync('portalid', id);
     wx.showActionSheet({
       itemList: ['查看详情'],
       success: function (res) {
         console.log(res.tapIndex)
         wx.navigateTo({
           url: '/pages/outdex/outdex',
         })
       },
       fail: function (res) {
         console.log(res.errMsg)
       }
     })
   }
 },
 
  f1: function () {
       wx.navigateTo({
         url: "/pages/index/index" //返回首页
       })
  }

  
})