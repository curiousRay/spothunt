
// 引入 QCloud 小程序增强 SDK
var qcloud = require('../../vendor/wafer2-client-sdk/index');

// 引入配置
var config = require('../../config');

// 显示繁忙提示
var showBusy = text => wx.showToast({
  title: text,
  icon: 'loading',
  duration: 10000
});

// 显示成功提示
var showSuccess = text => wx.showToast({
  title: text,
  icon: 'success'
});

// 显示失败提示
var showModel = (title, content) => {
  wx.hideToast();

  wx.showModal({
    title,
    content: JSON.stringify(content),
    showCancel: false
  });
};

var preview = function() {
  wx.previewImage({
    urls: ['http://chuantu.biz/t6/330/1529379054x-1404795852.png']
  })
};


Page({
  f0:function(event){
    wx.switchTab({
      url: "/pages/map/map"
    })
  },
  
  f1:function(){
    wx.showModal({
      title: 'SpotHunt',
      content: '感谢您的致函，我们将竭诚为您服务！',
      cancelText:'我要投诉',
      confirmText:'我要打赏',
      success:function(res){
        if(res.confirm){
          console.log("ok!")
          preview();
        }
        else if(res.cancel){
          wx.showToast({
            title: 'sorry暂未开放',
            icon: 'info',
            duration: 2000
          })
        }

      }
    })
  }
})