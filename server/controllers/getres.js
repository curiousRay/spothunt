const { mysql } = require('../qcloud')

function creatElem(id, title, lng, lat, dist, comment, specpic) {
  // 创建待发送回客户端的数组
  this.id = id;
  this.title = title;
  this.lng = lng;
  this.lat = lat;
  this.dist = dist;
  this.comment = comment;
  this.specpic = specpic;
}

module.exports = async ctx => {
  ctx.response.type = 'application/json';
  let query = ctx.query;
  let lng = query.lng;
  let lat = query.lat;
  lng = Number(lng);
  lng = Number(lat);
  let status = (lng >= -180 && lng <= 180 && lat >= -90 && lat <= 90)? "success": "fail";
  ctx.response.body = {};
  // lng<0 为西经，>0为东经
  // lat<0 为南纬，>0为北纬
  if (status == "fail") {
    ctx.response.body = {
      "status": "fail"
      };
  }
  else {
    let distlim = 9999999; // 展现周围20km半径内的据点
    await mysql ('cAuth').select ('*').from ('spots').then (res => {
      let show_list = new Array();
      for (let i = 0; res[i] != null; i ++) {
        let dist = 2 * 6371.393 * Math.asin(Math.sqrt(Math.pow(Math.sin(0.5 * (lat - res[i].lat) / 180 * Math.PI), 2) + Math.cos(lat / 180 * Math.PI) * Math.cos(res[i].lat / 180 * Math.PI) * Math.pow(Math.sin(0.5 * (lng - res[i].lng) / 180 * Math.PI), 2)));
        res[i].dist = dist.toFixed(3); // 计算的距离（千米数）取小数点后三位

        if (res[i].dist <= distlim && res[i].dist >= 0) {
          show_list.push (new creatElem(
            res[i].id, 
            res[i].title, 
            res[i].lng, 
            res[i].lat, 
            res[i].dist, 
            res[i].comment, 
            res[i].specpic ));
        }
      };

      var numlim = 10; // 返回指定数目的数组
      var compare = function (prop) {
        return function (obj1, obj2) {
          var val1 = obj1[prop];
          var val2 = obj2[prop]; 
          if (val1 < val2) {
            return -1;
          } else if (val1 > val2) {
            return 1;
          } else {
            return 0;
          }
        }
      }
      show_list.sort(compare("dist"));
      show_list = show_list.slice(0, numlim);
      console.log(show_list);

      ctx.response.body = {
        "status": "success",
        "data": show_list
      };
    });

  
  }
  
};



